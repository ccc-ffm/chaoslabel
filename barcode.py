#! /usr/bin/python2

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.units import mm
from reportlab.graphics.barcode import code128
from reportlab.pdfbase.pdfmetrics import stringWidth
from baluhn import generate
import os
import random
import sys
import argparse



argp = argparse.ArgumentParser(description='Frankfurter Chaosinventarlabelgenerator')
argp = argparse.ArgumentParser(version='0.3')
argp.add_argument('-f', action='store', metavar='first', type=int, default=1, help='first value -- number on 1st label - default: 1')
argp.add_argument('-c', action='store', metavar='count', type=int, default=48, help='count -- number of labels generated - default: 48')
argp.add_argument('-m', action='store_true', default=False, help='cutMarks -- print cutmarks on left and right side')
argp.add_argument('-g', action='store_true', default=False, help='grid -- print a grid for manual cutting or printer adjustment')
argp.add_argument('-d', action='store_true', default=False, help='tin foil hats counter-measures (de-energize barcodes)')
argp.add_argument('-o', action='store', metavar='outfile', type=str, default='chaoslabel.pdf', help='outfile -- name of generated pdf - default: chaoslabel.pdf')
argp.add_argument('-i', action='store', metavar='infile', type=str, default='', help='infile -- reads numbers from file rather then generating them')
argp.add_argument('-n', action='append', metavar='', type=int, nargs='+', help='list of space separeted numbers to use')


args = argp.parse_args()

labelcount = args.c

# create pdf object
c=canvas.Canvas(args.o, pagesize=landscape(A4))
c.setTitle("Frankfurter Chaosinventarlabel")

# set font and size for url
c.setFont("Helvetica", 8)

values = []

if args.n:
	values = args.n[0]
elif args.i:
	if os.path.isfile(args.i) and os.access(args.i, os.R_OK):
		with open(args.i) as infile:
			lines = infile.readlines()
			for line in lines:
				line = line.strip()
				if line.isdigit():
					values.append(line)
			if len(values) == 0:
				print "File \"%s\" contains no numbers" % (args.i)
				sys.exit(3)
	else :
		print "File \"%s\" not found or not readable" % (args.i)
		sys.exit(2)
else:
	values = range(args.f, args.f + args.c)

# set count to 0
count = 0

# loop for generating the pages
for run in range(labelcount / 48 + 1):

	# set font and size for url
	c.setFont("Helvetica", 8)

	# set line width for grid, cut marks and tin foil hat counter-measures
	c.setLineWidth(0.05)


	for xrow in range(1, 9):
		row = 9 - xrow
		for col in range(1, 7):

			# number to print on the barcode an generation of luhn checksum
			bctext=str(values[count]) + generate(str(values[count]))

			# generate code128 barcode
			barc=code128.Code128(bctext, barWidth=0.4 * mm, barHeight=12 * mm, humanReadable=True)

			# calculate position in grid
			x = 5 + ((col - 1) * 48)
			y = 10 + ((row - 1) * (25 + 1))

			# set offset for barcode
			barx = x - 6

			# guess width of barcode and adjust offset once again
			if len(bctext) == 2:
				offset = 12.5
			if len(bctext) == 3:
				offset = 10
			if len(bctext) == 4:
				offset = 12
			if len(bctext) == 5:
				offset = 8
			if len(bctext) == 6:
				offset = 10.5
			if len(bctext) == 7:
				offset = 5.5
			if len(bctext) == 8:
				offset = 8.5
			if len(bctext) == 9:
				offset = 3.5
			if len(bctext) == 10:
				offset = 6
			barx += offset

			# "print" barcode to pdf object
			barc.drawOn(c, barx * mm, y * mm)

			# "print" url to pdf object
			c.drawCentredString((x + 24) * mm, (y + 13) * mm,"https://inv.cccffm.de/i/" + bctext)

			# de-energize barcodes # for tin foil hats counter-measures!
			if args.d == True:
				c.line((barx + 5) * mm, (y + 1) * mm, ((barx + 55) - (2 * offset)) * mm, (y + 1) * mm)
			
			# generate vetical grid if grid = 1
			if args.g == True:
				if row == 8:
					c.line(x * mm, 0 , x * mm , 210 * mm)
			
			# inc counter
			count += 1

			# abort if count is reached
			if count == len(values):
				break

		# adjust rowheight for grid and cut marks
		y -= 7

		# generat cut marks if curmark == 1
		if args.m == True:
			c.line(0 * mm, y * mm, 6 * mm, y * mm)
			c.line(291 * mm, y * mm, 297 * mm, y * mm)

		# generate horizontal grid if grid == 1
		if args.g == True:
			c.line(0 * mm, y * mm, 297 * mm, y * mm)			

		if count == len(values):
			break

	#generate the very right vertical grid line
	if args.g == True:
		x = 5 + (6 * 48)	
		c.line(x * mm, 0 , x * mm , 210 * mm)

	if count == len(values):
		break

	# generate page
	c.showPage()

#save to file
c.save()

# show rendered pdf with evince
#os.system("evince " + args.o)
