# chaoslable
Script zum erzeugen der HQ Inventaraufkleber

## Usage
barcode.py [-h] [-v] [-f first] [-c count] [-m] [-g] [-d] [-o outfile] [-i infile] [-n  [...]]


-h - prints help
-f <int> - value of first label (must be integer) - default: 1
-c <int> - number of labels generated (must be integer) - default: 48
-m - print cut Marks
-g - print grid
-o <file> - output filename - default: chaoslabel.pdf
-d - tin foil hats counter-measures (de-energize)
-i <file> - reads numbers from specified file rather then generating them
-n [...] - list of space separeted numbers to use


### de-energize?!?
Ich habe auch keine Ahnung, aber die Aluhüte finden es toll!
https://www.youtube.com/watch?v=Om9fZp-jdQQ

## Abhängigkeiten

* Python 2.7
* reportlab 3.1.x for python2

## -i fileformat
Eine Zahl pro Zeile. Wenn in einer Zeile mehr als eine Zahl und ein Whitespace steht wird die Zeile übersrprungen.

### Beispiel
```
1
20
300
abc
4000
```
würde Etiketten mit den Werten 1, 20, 300 und 4000 ergeben